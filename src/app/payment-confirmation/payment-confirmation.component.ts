import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {AppService} from "../services/app.service";
import { ToastrService } from 'ngx-toastr';
import { TokenStorageService } from '../components/Authentication/services/token-storage.service';

@Component({
  selector: 'app-payment-confirmation',
  templateUrl: './payment-confirmation.component.html',
  styleUrls: ['./payment-confirmation.component.scss']
})
export class PaymentConfirmationComponent implements OnInit {

  trackingId: string;
  transactionRef: string;
  message: string;
  success = false;

  constructor(private router: ActivatedRoute,
    private appService: AppService, private tokenStorage: TokenStorageService,
    private toastr: ToastrService) { }

  ngOnInit() {
this.getPaymentRef();
  }

  getPaymentRef() {
    this.router.queryParams.subscribe(params => {
      this.transactionRef = params['txref'] ;
      this.trackingId = params['trackingId'];
      if(this.transactionRef) {
        this.getPaymentInfo(this.transactionRef, this.trackingId);
      }
   });
  
  }

  getPaymentInfo(transactionRef, trackingId) {
    this.appService.confirmPayments(transactionRef).subscribe(result => {
      this.toastr.success("Payment Successful.");
      this.success = true;
      if(trackingId) {
        this.message= "Payment Successful. Redirecting to order page";
        if(this.tokenStorage.getToken()){
         setTimeout(()=>{ window.location.href = '/delivery-requests/'+trackingId;}, 4000)
        } else {
        setTimeout(()=>{ window.location.href = '/home/delivery-requests/'+trackingId;}, 4000)
        }
        
      } else {
        this.message= "Payment Successful. ";
      }
    }, error => {
      this.toastr.error("Payment Unsuccessful. ");
      this.success = false;
      if(trackingId) {
        this.message = "Payment Unsuccessful! Redirecting to order page";
        if(this.tokenStorage.getToken()){
          setTimeout(()=>{ window.location.href = '/delivery-requests/'+trackingId;}, 4000)
        } else {
          setTimeout(()=>{ window.location.href = '/home/delivery-requests/'+trackingId;}, 4000)
        }
      } else {
        this.message= "Payment Unsuccessful. ";
      }
    });
  }

  isLoggedIn(){
    return this.tokenStorage.getToken() ? true : false;
  }

}
