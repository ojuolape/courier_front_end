/// <reference types="@types/googlemaps" />
import {Component, ViewChild, ChangeDetectorRef, ElementRef} from '@angular/core';
import {DEFAULT_INTERRUPTSOURCES, Idle} from "@ng-idle/core";
import {Keepalive} from "@ng-idle/keepalive";
import {BsModalRef, BsModalService, ModalDirective} from "ngx-bootstrap";
import {Router} from "@angular/router";
import {AuthService} from "./components/Authentication/services/auth.service";
import {TokenStorageService} from "./components/Authentication/services/token-storage.service";
import { MessagingService } from './services/messaging.service';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { FcmNotificationModel } from './components/model/fcm-notification';
import { VehicleService } from './services/vehicle.service';
import { AppService } from './services/app.service';

declare var swal: any;
declare var $: any;
declare var jQuery: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'naytifbox';
  idleState = 'Not started.';
  timedOut = false;
  lastPing?: Date = null;
  message: FcmNotificationModel;
  notificationMessage = '';
  public modalRef: BsModalRef;
  currentLat: any;
  currentLong: any;
  firstRole: string;

  @ViewChild('childModal', { static: false }) childModal: ModalDirective;
  @ViewChild('notificationModal', { static: false }) notificationModal: ModalDirective;

  @ViewChild('audioOption', { static: false }) audioPlayerRef: ElementRef;

  constructor(private idle: Idle, private keepalive: Keepalive,
    private tokenStorageService: TokenStorageService, private angularFireMessaging: AngularFireMessaging,
              private service: AppService, private appService: AuthService) {
    // sets an idle timeout of 5 seconds, for testing purposes.
    idle.setIdle(900);
    // sets a timeout period of 5 seconds. after 10 seconds of inactivity, the user will be considered timed out.
    idle.setTimeout(5);
    // sets the default interrupts, in this case, things like clicks, scrolls, touches to the document
    idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

    idle.onIdleEnd.subscribe(() => {
      this.idleState = 'No longer idle.'
      console.log(this.idleState);
      this.reset();
    });

    idle.onTimeout.subscribe(() => {
      this.childModal.hide();
      this.idleState = 'Timed out!';
      this.timedOut = true;
      console.log(this.idleState);
      this.logout();
      //this.router.navigate(['/']);
    });

    idle.onIdleStart.subscribe(() => {
      this.idleState = 'You\'ve gone idle!'
      console.log(this.idleState);
      this.childModal.show();
    });

    idle.onTimeoutWarning.subscribe((countdown) => {
      this.idleState = 'You will time out in ' + countdown + ' seconds!'
      console.log(this.idleState);
    });

    // sets the ping interval to 15 seconds
    keepalive.interval(15);

    keepalive.onPing.subscribe(() => this.lastPing = new Date());

    this.appService.getUserLoggedIn().subscribe(userLoggedIn => {
      if (userLoggedIn) {
        idle.watch()
        this.timedOut = false;
      } else {
        idle.stop();
      }
    })

    this.firstRole = this.tokenStorageService.getAuthorities()[0];
    if(this.firstRole === 'DRIVER'){
        this.trackMe();
        console.log('in riders')
    }

    // this.reset();
  }

  ngOnInit() {  
   
    this.angularFireMessaging.messages.subscribe(
      (payload) => {
        this.message = payload as FcmNotificationModel;
        this.audioPlayerRef.nativeElement.play();
        if(this.message.data && this.message.data.notificationType){

            if(this.message.data.notificationType === 'NEW_DELIVERY_REQUEST' && this.message.data.trackingId){
              const trackingId = this.message.data.trackingId;
              swal({
                title: this.message.notification.title,
                text: this.message.notification.body,
                type: "success",
                showCancelButton: true,
                confirmButtonColor: "#dc3545",
                confirmButtonText: "Open Request",
                cancelButtonText: "Ok",
                closeOnConfirm: false,
                closeOnCancel: true
            }, function (isConfirm) {
                if (isConfirm) {
                   window.location.href= `/delivery-requests/${trackingId}`
                } 
            });
              return;
            }
        } 
        swal(this.message.notification.title, this.message.notification.body, "success");
       
      })

    
  }

  reset() {
    this.idle.watch();
    //xthis.idleState = 'Started.';
    this.timedOut = false;
  }

  hideChildModal(): void {
    this.childModal.hide();
  }

  hideNotificationModal(): void {
    this.notificationModal.hide();
  }

  stay() {
    this.childModal.hide();
    this.reset();
  }

  logout() {
    this.childModal.hide();
    this.appService.logout();
  }

  trackMe() {
    if (navigator.geolocation) {
      navigator.geolocation.watchPosition((position) => {
        this.showTrackingPosition(position);
      });
    } else {
      alert("Geolocation is not supported by this browser.");
    }
  }

  showTrackingPosition(position) {
    console.log(`tracking postion:  ${position.coords.latitude} - ${position.coords.longitude}`);
    this.currentLat = position.coords.latitude;
    this.currentLong = position.coords.longitude;

    let location = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
    console.log(location)
    const data = {longitude: this.currentLong,
                  latitude: this.currentLat,
                  address: ""};
    this.service.sendLocation(data).subscribe(response => {
       console.log("location sent");
     });

  }
}
