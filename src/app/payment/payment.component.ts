import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import { TokenStorageService } from '../components/Authentication/services/token-storage.service';
import { environment } from 'src/environments/environment';
declare var $: any;
declare const getpaidSetup: any; 

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {

  amount: number;
  trackingId: string;
  txref: string;
  name: string;
  phone: string;
  goBackLink: string;
  closed = false;

  constructor(private router: ActivatedRoute,
    private tokenStorage: TokenStorageService) { }

  ngOnInit() {
    this.router.queryParams.subscribe(res => {
      this.trackingId = res["trackingId"];
      this.txref = res["transactionRef"];
    })
    
    this.goBackLink = "/payment-confirmation?txref="+this.txref+"&trackingId="+this.trackingId;
    if(!this.tokenStorage.getToken()){
     // this.goBackLink = "/home/delivery-requests/"+this.trackingId;
    }
    
  $( document ).ready(function() {

    function getParameterByName(name) {
      var url = window.location.href;
      name = name.replace(/[\[\]]/g, '\\$&');
      var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
          results = regex.exec(url);
      if (!results) return null;
      if (!results[2]) return '';
      return decodeURIComponent(results[2].replace(/\+/g, ' '));
  }

      var x = getpaidSetup({
        PBFPubKey: environment.flutterwavePubKey,
        customer_email: getParameterByName('email') || "naytifbox@gmail.com",
        amount: getParameterByName('amount'),
        customer_phone: getParameterByName('phone') || "2348033187237",
        currency: "NGN",
        txref: getParameterByName('transactionRef'),
        onclose: function() {
          var c = document.getElementById('cancelled');
          c.style.display = "block";
          //window.location.href = "payment-failed?transactionRef="+getParameterByName('transactionRef');
        },
        callback: function(response) {
            var txref = response.tx.txRef; // collect txRef returned and pass to a 					server page to complete status check.
            if (
                response.tx.chargeResponseCode == "00" ||
                response.tx.chargeResponseCode == "0"
            ) {
              window.location.href = "payment-confirmation?txref="+txref+"&trackingId="+getParameterByName('trackingId');
                  // redirect to a success page
              } else {
                window.location.href = "payment-confirmation?txref="+txref+"&trackingId="+getParameterByName('trackingId');
               // window.location.href = "payment-failed?txref="+txref+"&trackingId="+getParameterByName('trackingId');
  // redirect to a failure page.
            }

           // x.close(); // use this to close the modal immediately after payment.
        }
    });
      //return false;
    });
}

isLoggedIn(){
  return this.tokenStorage.getToken() ? true : false;
}
}
