import { Component, OnInit } from '@angular/core';
import {ConstNameService} from '../../../services/const-name.service';
import {AuthService} from "../../../components/Authentication/services/auth.service";
import { TokenStorageService } from 'src/app/components/Authentication/services/token-storage.service';

@Component({
  selector: 'app-page-topbar',
  templateUrl: './page-topbar.component.html',
  styleUrls: ['./page-topbar.component.scss']
})
export class PageTopbarComponent implements OnInit {

  url: string;
  
  constructor(private authService: AuthService,
    private constName: ConstNameService,
    private tokenStorage: TokenStorageService
  ) { }

  ngOnInit() {
    this.url = this.constName.baseImage.file_img_url;
  }

  logout() {
    this.authService.logout();
  }

  isLoggedIn(){
    return this.tokenStorage.getToken() ? true : false;
  }

}
