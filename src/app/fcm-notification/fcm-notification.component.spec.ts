import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FcmNotificationComponent } from './fcm-notification.component';

describe('FcmNotificationComponent', () => {
  let component: FcmNotificationComponent;
  let fixture: ComponentFixture<FcmNotificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FcmNotificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FcmNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
