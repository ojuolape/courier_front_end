import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {SignUpInfo} from "../components/Authentication/services/signup-info";
import {RegisterUser} from "../components/model/register-user";

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private httpClient: HttpClient) { }

  public search(params) {
    return this.httpClient.get<any>(`${environment.baseUrl}/users`, {
      params: params
    });
  }

  public register(data: RegisterUser) {
    return this.httpClient.post<any>(`${environment.baseUrl}/users/register`, data);
  }

  public createDeliveryRequest(data: any) {
    return this.httpClient.post<any>(`${environment.baseUrl}/api/orders`, data);
  }

  public createVendorDeliveryRequest(data: any) {
    return this.httpClient.post<any>(`${environment.baseUrl}/api/orders/vendor`, data);
  }

  public getDeliveryRequestByTrackingId(trackingId: string){
    return this.httpClient.get<any>(`${environment.baseUrl}/api/orders/${trackingId}`, {});
  }

  public addDriverToDelivery(trackingId: string, driverId: number){
    return this.httpClient.post<any>(`${environment.baseUrl}/api/orders/${trackingId}/drivers/${driverId}`, {});
  }

  public updateDeliveryPrice(trackingId: string, amount: number){
    return this.httpClient.post<any>(`${environment.baseUrl}/api/orders/${trackingId}?amount=${amount}`, {
     amount: amount
    });
  }

  public getAdminDashboardStats() {
    return this.httpClient.get<any>(`${environment.baseUrl}/stats/admin/orders`, {

    });
  }

  public updateDeliveryStatus(trackingId: string, status: string){
    return this.httpClient.post<any>(`${environment.baseUrl}/api/orders/${trackingId}/status`, {
      deliveryStatus: status
    });
  }

  public createDeliveryRequestExtranet(data: any) {
    return this.httpClient.post<any>(`${environment.baseUrl}/api/orders/extranet`, data);
  }

  public confirmPayments(transactionRef: any){
    return this.httpClient.post<any>(`${environment.baseUrl}/api/orders/payment/${transactionRef}`, 
    {});
  
  }
  
  public sendLocation(data: any) {
    return this.httpClient.post<any>(`${environment.baseUrl}/gps-locations`, data);
  }

  public getUserInfoByUserId(userId: string) {
    return this.httpClient.get<any>(`${environment.baseUrl}/users/${userId}`, {
    });
  }

  public fetchUserLocation(userId: string){
    return this.httpClient.get<any[]>(`${environment.baseUrl}/gps-locations/${userId}`,{});
  }
}
