import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(private httpClient: HttpClient) { }

  public getGpsDistance(origin, destination) {
    origin = encodeURIComponent(origin);
    destination = encodeURIComponent(destination);
    return this.httpClient.get<any>(`${environment.baseUrl}/api/orders/estimate?origin=${origin}&destination=${destination}`, {
      params: {}
    });
  }
  public searchDriver(limit, offset, filter) {
    return this.httpClient.get<any>(`${environment.baseUrl}/drivers`, {
      params: {limit: limit, pageNumber: offset, q: filter}
    });
  }

  public searchDeliveries(param) {
    return this.httpClient.get<any>(`${environment.baseUrl}/api/orders`, {
      params: param
    });
  }

}
