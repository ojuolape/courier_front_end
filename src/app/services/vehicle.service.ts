import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
@Injectable({
  providedIn: 'root'
})
export class VehicleService {

  constructor(private httpClient: HttpClient) { }

  public search(limit, pageNumber, filter) {
    return this.httpClient.get<any>(`${environment.baseUrl}/vehicles`, {
      params: {limit: limit, pageNumber: pageNumber, q: filter}
    });
  }

  public create(data: any) {
    return this.httpClient.post<any>(`${environment.baseUrl}/vehicles`, data);
  }

  public createDriver(data: any) {
    return this.httpClient.post<any>(`${environment.baseUrl}/drivers`, data);
  }

  
}
