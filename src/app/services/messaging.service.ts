
import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { mergeMapTo } from 'rxjs/operators';
import { take } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs'
import { AuthService } from '../components/Authentication/services/auth.service';
import { TokenStorageService } from '../components/Authentication/services/token-storage.service';
import { FcmNotificationModel } from '../components/model/fcm-notification';

@Injectable()
export class MessagingService {

  currentMessage = new BehaviorSubject(null);

  constructor(
      private authService: AuthService,
      private tokenStorage: TokenStorageService,
    private angularFireDB: AngularFireDatabase,
    private angularFireAuth: AngularFireAuth,
    private angularFireMessaging: AngularFireMessaging) {
    this.angularFireMessaging.messaging.subscribe(
      (_messaging) => {     
        // _messaging._next = (payload: any) => {
        //     console.log(payload);
            
        // };
    _messaging.onTokenRefresh = _messaging.onTokenRefresh.bind(_messaging);
    _messaging.onTokenRefresh(async () => {
        const newToken = await _messaging.getToken();
        this.authService.registerFcmToken(newToken);
    });
      }
    )
  }

  /**
   * update token in firebase database
   * 
   * @param userId userId as a key 
   * @param token token as a value
   */
  updateToken(userId, token) {
    // we can change this function to request our backend service
    this.angularFireAuth.authState.pipe(take(1)).subscribe(
      () => {
        const data = {};
        data[userId] = token
        this.angularFireDB.object('fcmTokens/').update(data)
      })
  }

  /**
   * request permission for notification from firebase cloud messaging
   * 
   * @param userId userId
   */
  requestPermission() {
    this.angularFireMessaging.requestToken.subscribe(
      (token) => {
        console.log(token);
        this.authService.registerFcmToken(token);
       
      },
      (err) => {
        console.error('Unable to get permission to notify.', err);
      }
    );
  }


  /**
   * hook method when new notification received in foreground
   */
  receiveMessage() {
    this.angularFireMessaging.messages.subscribe(
      (payload) => {
        console.log("new message received. ", payload);
        this.currentMessage.next(payload);
      })
  }


  showMessage(){

  }
}
