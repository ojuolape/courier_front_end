/// <reference types="@types/googlemaps" />
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AppService } from 'src/app/services/app.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-driver-profile',
  templateUrl: './driver-profile.component.html',
  styleUrls: ['./driver-profile.component.scss']
})
export class DriverProfileComponent implements OnInit {

  @ViewChild('gmap', { static: true }) gmapElement: ElementRef;
  map: google.maps.Map;
  marker: google.maps.Marker;
  user: any;
  driver = {latitude:9.072264, longitude:7.491302};
  lastLocation = "";
  private geoCoder;
  latlng = [];
  
  constructor(private appService: AppService, 
    private toastService: ToastrService,
    private router: ActivatedRoute) { }

  ngOnInit() {
    try{
    this.geoCoder = new google.maps.Geocoder;
    var mapProp = {
      center: new google.maps.LatLng(9.0158751,7.4778468),
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    this.map = new google.maps.Map(this.gmapElement.nativeElement, mapProp);
  }catch(err){
    console.log(err)
  }
    this.router.params
      .pipe(map(params => params['trackingId']))
      .subscribe(trackingId => {

          this.fetchDriverInfo(trackingId);
          this.fetchPreviousLocations(trackingId);
      });
  }

  fetchDriverInfo(driverId){
    this.appService.getUserInfoByUserId(driverId).subscribe(response => {
        this.user = response.userInfo;
        this.driver = response.driverInfo;
        if(this.driver.latitude && this.driver.longitude){
          this.getAddress((this.driver.latitude), (this.driver.longitude));
        }
    }, err =>{
        this.toastService.error("Could not fetch user information"); 
    })
  }

  fetchPreviousLocations(driverId){
    this.appService.fetchUserLocation(driverId).subscribe(response => {
      this.latlng = response;
    }, err => {
      this.toastService.error("Could not fetch previous locations!");
    })
  }
  getAddress(latitude, longitude) {
    this.geoCoder.geocode({ 'location': { lat: parseFloat(latitude), lng: parseFloat(longitude) } }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          this.lastLocation =  results[0].formatted_address;
          console.log(this.lastLocation)
        } 
        return "";
      } 
      return "";
 
    });
  }
  showTrackingPosition(lat, lng) {
    let location = new google.maps.LatLng(lat, lng);
    this.map.panTo(location);

    if (!this.marker) {
      this.marker = new google.maps.Marker({
        position: location,
        map: this.map,
        title: 'Got you!'
      });
    }
    else {
      this.marker.setPosition(location);
    }
  }
}
