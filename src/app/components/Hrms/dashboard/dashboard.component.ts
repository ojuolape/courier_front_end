import {Component, Input, OnInit} from '@angular/core';
import {AuthService} from "../../Authentication/services/auth.service";
import {TokenStorageService} from "../../Authentication/services/token-storage.service";
import { MessagingService } from 'src/app/services/messaging.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {


  firstRole: any;
  constructor(
    private authService: AuthService,
    private messagingService: MessagingService,
    private tokenStorageService: TokenStorageService) {
  }

  ngOnInit() {
    this.firstRole = this.tokenStorageService.getAuthorities()[0];
    this.messagingService.requestPermission()
    this.messagingService.receiveMessage()
  }

  containsRole(roles: string[], roleName: string){
    if (roles.indexOf(roleName) > -1) {
      return true;
    } else {
      return false;
    }
  }

}
