import {Component, Input, OnInit} from '@angular/core';
import {ConstNameService} from '../../../services/const-name.service';
import {QueryResults} from "../../model/query-result";
import {HttpErrorResponse} from "@angular/common/http";
import {ToastrService} from "ngx-toastr";
import {AppService} from "../../../services/app.service";
import {UserModel} from "../../model/user";
import {RegisterUser} from "../../model/register-user";
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  itemsPerPage = 10;
  currentPage = 1;
  working = false;
  registering = false;
  queryResult: QueryResults<UserModel>;
  url: string;
  searchFilter :string = "";
  roleName: string = "";
  userInfo: RegisterUser;
  form: any = {role: ''};
  @Input() navTab: string = 'user-list';
  constructor(
    private constName: ConstNameService,
    private toastService: ToastrService,
    private userService: AppService
  ) { }

  ngOnInit() {
    this.url = this.constName.baseImage.file_img_url;
    this.onPageChanged({page: 1});
  }

  changeNavTab(tab: string) {
    this.navTab = tab;
  }

  get totalCount() {
    return this.queryResult ? this.queryResult.total : 0;
  }

  get offset() {
    return this.queryResult ? this.queryResult.offset : 0;
  }

  onSubmit() {
    this.onPageChanged({page: 1});
  }

  onPageLengthChanged(length: number) {
    this.itemsPerPage = length;
    this.onPageChanged({page: 1});
  }

  onPageChanged(event: { page: number }) {
    this.currentPage = event.page;
    const offset = (event.page - 1) * this.itemsPerPage;
    this.working = true;
    const param = {limit: this.itemsPerPage, offset: (this.currentPage - 1),
       q: this.searchFilter, role: this.roleName}
    this.userService.search(param)
      .subscribe(result => {
        console.log(result);
        this.working = false;
        this.queryResult = result;

      }, (err: HttpErrorResponse) => {
        this.working = false;
        this.toastService.error('Unable to search at the moment due to an error!');
      });
  }

  registerUser(ngForm: NgForm){

    if(this.registering){
      return;
    }
    if(ngForm.form.invalid){
      ngForm.form.markAllAsTouched();
      this.toastService.error('Fill all required fields');
      return;
    }

    if(this.form.password !== this.form.confirmPassword){
      return;
    }
    this.registering = true;
    this.userInfo = new RegisterUser(
      this.form.name,
      [this.form.role],
      this.form.email,
      this.form.password,
      this.form.phoneNumber);

    this.userService.register(this.userInfo).subscribe(
      data => {
        this.registering = false;
        this.toastService.success('Registration was successful');
        //ngForm.reset();
        ngForm.form.reset();
        ngForm.form.markAsPristine();

      },
      error => {
        this.registering = false;
        const errorMessage = error.error.errors ? error.error.errors[0].defaultMessage : ( error.error.message ? error.error.message :'Could not process request, try again later');
        this.toastService.error(errorMessage);
      }
    );
  }
}
