import {Component, Input, OnInit, TemplateRef} from '@angular/core';
import {ConstNameService} from '../../../services/const-name.service';
import {VehicleModel} from "../../model/vehicle";
import {QueryResults} from "../../model/query-result";
import {ToastrService} from "ngx-toastr";
import {VehicleService} from "../../../services/vehicle.service";
import {HttpErrorResponse} from "@angular/common/http";
import {NgForm} from "@angular/forms";
import {RegisterVehicle} from "../../model/new-vehicle";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
declare var jquery: any;
declare var $: any;
declare var require: any;

@Component({
  selector: 'app-vehicles',
  templateUrl: './vehicles.component.html',
  styleUrls: ['./vehicles.component.scss']
})
export class VehiclesComponent implements OnInit {

  itemsPerPage = 10;
  currentPage = 1;
  working = false;
  registering = false;
  queryResult: QueryResults<VehicleModel>;
  searchFilter :string = "";
  form: any = {vehicleType: 'MOTORCYCLE'};
  url: string;
  vehicleInfo: RegisterVehicle;
  driverForm: any = {vehicleNumber : ""};
  modalRef: BsModalRef;
  @Input() navTab: string = 'Vehicle-list';
  constructor(
    private constName: ConstNameService,
    private toastService: ToastrService,
    private vehicleService: VehicleService,
    private modalService: BsModalService
  ) { }

  ngOnInit() {
    this.url = this.constName.baseImage.file_img_url;
    $( document ).ready(function() {
      const DIV_CARD = 'div.card';
      $('[data-toggle="card-remove"]').on('click', function(e) {
        const card = $(this).closest(DIV_CARD);
        card.remove();
        e.preventDefault();
        return false;
      });
      /** Function for collapse card */
      $('[data-toggle="card-collapse"]').on('click', function(e) {
        const card = $(this).closest(DIV_CARD);
        if (card.hasClass('card-collapsed')) {
          card.removeClass('card-collapsed');
        } else {
          card.addClass('card-collapsed');
        }
        e.preventDefault();
        return false;
      });

    });
    this.onPageChanged({page: 1});

  }

  changeNavTab(tab: string) {
    this.navTab = tab;
  }

  get totalCount() {
    return this.queryResult ? this.queryResult.total : 0;
  }

  get offset() {
    return this.queryResult ? ((this.currentPage - 1) * this.itemsPerPage) : 0;
  }

  onSubmit() {
    this.onPageChanged({page: 1});
  }

  onPageLengthChanged(length: number) {
    this.itemsPerPage = length;
    this.onPageChanged({page: 1});
  }

  onPageChanged(event: { page: number }) {
    this.currentPage = event.page;
    const offset = (event.page - 1) * this.itemsPerPage;
    this.working = true;

    this.vehicleService.search(this.itemsPerPage , (this.currentPage - 1), this.searchFilter )
      .subscribe(result => {
        console.log(result);
        this.working = false;
        this.queryResult = result;

      }, (err: HttpErrorResponse) => {
        this.working = false;
        this.toastService.error('Unable to search at the moment due to an error!');
      });
  }

  createVehicle(ngForm: NgForm){

    if(this.registering){
      return;
    }
    if(ngForm.form.invalid){
      ngForm.form.markAllAsTouched();
      this.toastService.error('Fill all required fields');
      return;
    }
    this.registering = true;
    this.vehicleInfo = new RegisterVehicle(this.form.vehicleNumber, this.form.vehicleType);

    this.vehicleService.create(this.vehicleInfo).subscribe(
      data => {
        this.registering = false;
        this.toastService.success('Vehicle was created successfully');
        ngForm.reset();
        ngForm.form.markAsPristine();
        this.onPageChanged({page: 1});
      },
      error => {
        this.registering = false;
        const errorMessage = error.error.errors ? error.error.errors[0].defaultMessage : ( error.error.message ? error.error.message :'Could not process request, try again later');
        this.toastService.error(errorMessage);
      }
    );
  }

  openDriverModal(template: TemplateRef<any>, vehicleNumber: string){
  console.log('opend drvier modal');
    this.driverForm.vehicleNumber = vehicleNumber;
    this.modalRef = this.modalService.show(template);
  }

  createDriver(ngForm: NgForm){
    if(this.registering){
      return;
    }
    if(ngForm.form.invalid){
      ngForm.form.markAllAsTouched();
      this.toastService.error('Fill all required fields');
      return;
    }
    this.registering = true;
    const data = {
      username: this.driverForm.username,
      emailAddress: this.driverForm.email,
      phoneNumber: this.driverForm.phoneNumber,
      name: this.driverForm.name,
      vehicleNumber: this.driverForm.vehicleNumber
    };

    this.vehicleService.createDriver(data).subscribe(
      data => {
        this.registering = false;
        this.toastService.success('Driver was created successfully');
        ngForm.reset();
        ngForm.form.markAsPristine();
        this.onPageChanged({page: 1});
        this.modalRef.hide();
      },
      error => {
        this.registering = false;
        const errorMessage = error.error.errors ? error.error.errors[0].defaultMessage : ( error.error.message ? error.error.message :'Could not process request, try again later');
        this.toastService.error(errorMessage);
      }
    );
  }

}
