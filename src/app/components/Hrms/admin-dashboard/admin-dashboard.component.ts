import {Component, Input, OnInit} from '@angular/core';
import {ApexAxisChartSeries, ApexChart, ApexNonAxisChartSeries, ChartComponent} from 'ng-apexcharts';
import ApexCharts from 'apexcharts/dist/apexcharts.common.js';
import {ConstNameService} from '../../../services/const-name.service';
import {SearchService} from "../../../services/search.service";
import {QueryResults} from "../../model/query-result";
import {HttpErrorResponse} from "@angular/common/http";
import {AppService} from "../../../services/app.service";
import {TokenStorageService} from "../../Authentication/services/token-storage.service";
declare var jquery: any;
declare var $: any;
declare var require: any;
@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.scss']
})
export class AdminDashboardComponent implements OnInit {

  url: string;
  working = false;
  queryResult: QueryResults<any>;
  stats: any = {};
  displayName: string;
  constructor(
    private searchService: SearchService,
    private appService: AppService,
    private storageService: TokenStorageService
  ) {
  }

  ngOnInit() {
    this.fetchLatestDeliveries();
    this.fetchDeliveriesStats();
    this.displayName = this.storageService.getDisplayName();
  }

  fetchLatestDeliveries(){
    this.working = false;
    const params = {limit: 4, pageNumber: 0}
    this.searchService.searchDeliveries(params )
      .subscribe(result => {
        this.working = false;
        this.queryResult = result;

      }, (err: HttpErrorResponse) => {
        this.working = false;
      });
  }

  fetchDeliveriesStats(){
    this.appService.getAdminDashboardStats().subscribe(response => {
      this.stats = response;
    })
  }
}
