import { NgModule } from '@angular/core';
import { NewAdminDeliveryRequestComponent } from './new-admin-delivery-request/new-admin-delivery-request.component';
import { ViewAdminDeliveryRequestWidgetComponent } from './widgets/view-admin-delivery-request-widget/view-admin-delivery-request-widget.component';
import { ViewDeliveryRequestWidgetComponent } from './widgets/view-delivery-request-widget/view-delivery-request-widget.component';
import { ViewDriverDeliveryRequestWidgetComponent } from './widgets/view-driver-delivery-request-widget/view-driver-delivery-request-widget.component';
import { DeliveryStatusUpdateWidgetComponent } from './widgets/delivery-status-update-widget/delivery-status-update-widget.component';

@NgModule({
  declarations: [],
  imports: [
  ]
})
export class DeliveryRequestModule { }
