import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebViewDeliveryRequestComponent } from './web-view-delivery-request.component';

describe('WebViewDeliveryRequestComponent', () => {
  let component: WebViewDeliveryRequestComponent;
  let fixture: ComponentFixture<WebViewDeliveryRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebViewDeliveryRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebViewDeliveryRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
