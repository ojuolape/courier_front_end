import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppService } from 'src/app/services/app.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-web-view-delivery-request',
  templateUrl: './web-view-delivery-request.component.html',
  styleUrls: ['./web-view-delivery-request.component.scss']
})
export class WebViewDeliveryRequestComponent implements OnInit {

  delivery: any;
paymentRef: any;
message: string;
driverAssignedDate: Date;
  inTransitDate: Date;
  deliveredDate: Date;
  constructor(private router: ActivatedRoute,
    private appService: AppService) { }

  ngOnInit() {
    this.router.params
      .pipe(map(params => params['trackingId']))
      .subscribe(trackingId => {

          this.getDeliveryInfo(trackingId);
      });
  }

  getDeliveryInfo(trackingId) {
    this.appService.getDeliveryRequestByTrackingId(trackingId).subscribe(result => {
      this.delivery = result;
      this.driverAssignedDate = this.getDeliveryHistoryTime('DRIVER_ASSIGNED');
      this.inTransitDate = this.getDeliveryHistoryTime('IN_TRANSIT');
      this.deliveredDate = this.getDeliveryHistoryTime('DELIVERED')
    }, error => {
      this.message = "Delivery request with tracking ID "+trackingId+" does not exist.";
  });
  }

  getDeliveryHistoryTime(status){
    if(!this.delivery){
      return null;
    }
    const date = this.delivery.deliveryHistory.filter(x => x.status === status).map(x => x.dateCreated);
    return date && date != '' ? date : null;
    
  }
}
