import { Component, OnInit, Input } from '@angular/core';
import {AppService} from "../../../../../services/app.service";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-delivery-status-update-widget',
  templateUrl: './delivery-status-update-widget.component.html',
  styleUrls: ['./delivery-status-update-widget.component.scss']
})
export class DeliveryStatusUpdateWidgetComponent implements OnInit {

  private isLoading = false;
  @Input() delivery: any;
  constructor(private appService: AppService,
              private toastService: ToastrService) { }

  ngOnInit() {
  }

  updateStatus(status: string){
    this.appService.updateDeliveryStatus(this.delivery.trackingId, status )
      .subscribe(result => {
        this.getDeliveryInfo(this.delivery.trackingId);
        this.isLoading = false;
        this.toastService.success('Delivery status updated successfully');
        window.location.reload();
      }, (err) => {
        this.isLoading = false;
        this.toastService.error('Unable to update delivery due to an error, try again later.');
      });
  }

  getDeliveryInfo(trackingId) {
    this.appService.getDeliveryRequestByTrackingId(trackingId).subscribe(result => {
      this.delivery = result;
    });
  }
}
