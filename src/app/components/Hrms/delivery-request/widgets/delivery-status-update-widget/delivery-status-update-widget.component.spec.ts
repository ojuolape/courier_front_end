import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveryStatusUpdateWidgetComponent } from './delivery-status-update-widget.component';

describe('DeliveryStatusUpdateWidgetComponent', () => {
  let component: DeliveryStatusUpdateWidgetComponent;
  let fixture: ComponentFixture<DeliveryStatusUpdateWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliveryStatusUpdateWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveryStatusUpdateWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
