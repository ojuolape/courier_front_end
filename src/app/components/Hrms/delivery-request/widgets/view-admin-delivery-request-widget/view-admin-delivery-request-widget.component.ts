import { Component, OnInit, Input, TemplateRef } from '@angular/core';
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {QueryResults} from "../../../../model/query-result";
import {VehicleModel} from "../../../../model/vehicle";
import {HttpErrorResponse} from "@angular/common/http";
import {SearchService} from "../../../../../services/search.service";
import {ToastrService} from "ngx-toastr";
import {AppService} from "../../../../../services/app.service";
@Component({
  selector: 'app-view-admin-delivery-request-widget',
  templateUrl: './view-admin-delivery-request-widget.component.html',
  styleUrls: ['./view-admin-delivery-request-widget.component.scss']
})
export class ViewAdminDeliveryRequestWidgetComponent implements OnInit {

  itemsPerPage = 10;
  currentPage = 1;
  assigningDriver = false;
  editingAmount = false;
  isEditAmount = false;
  amount: number;
  queryResult: QueryResults<VehicleModel>;
  modalRef: BsModalRef;
  driverForm: any = {};
  searching = false;

  @Input() delivery: any;
  constructor(
    private searchService: SearchService,
    private toastService: ToastrService,
    private appService: AppService,
    private modalService: BsModalService,) { }

  ngOnInit() {
    this.amount = this.delivery.amount/100.0;
   
  }

  toggleAmountEdit(){
    this.isEditAmount = !this.isEditAmount;
  }

  get totalCount() {
    return this.queryResult ? this.queryResult.total : 0;
  }

  get offset() {
    return this.queryResult ? ((this.currentPage - 1) * this.itemsPerPage) : 0;
  }

  onSubmit() {
    this.onPageChanged({page: 1});
  }

  onPageChanged(event: { page: number }) {
    this.currentPage = event.page;
    const offset = (event.page - 1) * this.itemsPerPage;
    this.searching = true;

    this.searchService.searchDriver(this.itemsPerPage , (this.currentPage - 1), this.driverForm.name )
      .subscribe(result => {
        console.log(result);
        this.searching = false;
        this.queryResult = result;

      }, (err: HttpErrorResponse) => {
        this.searching = false;
        this.toastService.error('Unable to search at the moment due to an error!');
      });
  }

  openDriverModal(template: TemplateRef<any>){

    this.driverForm.name = '';
    this.modalRef = this.modalService.show(template);
    this.onPageChanged({page: 1});
  }

  assignDriver(driver){
    if(!this.delivery.amount || this.delivery.amount == 0){
      this.toastService.error('Update delivery amount before assigning a driver');
      return;
    }
    this.assigningDriver = true;
    this.appService.addDriverToDelivery(this.delivery.trackingId, driver.id )
      .subscribe(result => {
        this.getDeliveryInfo(this.delivery.trackingId);
        this.assigningDriver = false;
        this.toastService.success('Driver assigned successfully');
        this.modalRef.hide();
      }, (res: HttpErrorResponse) => {
        this.assigningDriver = false;
        if (res.status === 401 || res.status === 400) {
          this.toastService.error(res.error.message);
        } else if (res.status < 1) {
          this.toastService.error('Failed to contact server. Please check your internet connection');
        } else {
          this.toastService.error('Unable to assign driver to delivery due to an error, try again later.');
        }
      });

  }

  processAmountEdit(){
    this.editingAmount = true;
    this.appService.updateDeliveryPrice(this.delivery.trackingId, this.amount )
      .subscribe(result => {
        this.getDeliveryInfo(this.delivery.trackingId);
        this.toastService.success('Price updated successfully');
        this.isEditAmount = false;
      }, (err: HttpErrorResponse) => {
        this.toastService.error('Unable to update delivery price due to an error, try again later.');
      });
    this.editingAmount = false;
  }

  getDeliveryInfo(trackingId) {
    this.appService.getDeliveryRequestByTrackingId(trackingId).subscribe(result => {
      this.delivery = result;
    });
  }
}
