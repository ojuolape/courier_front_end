import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewAdminDeliveryRequestWidgetComponent } from './view-admin-delivery-request-widget.component';

describe('ViewAdminDeliveryRequestWidgetComponent', () => {
  let component: ViewAdminDeliveryRequestWidgetComponent;
  let fixture: ComponentFixture<ViewAdminDeliveryRequestWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewAdminDeliveryRequestWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewAdminDeliveryRequestWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
