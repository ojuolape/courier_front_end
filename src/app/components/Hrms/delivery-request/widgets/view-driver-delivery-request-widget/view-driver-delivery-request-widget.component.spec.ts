import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewDriverDeliveryRequestWidgetComponent } from './view-driver-delivery-request-widget.component';

describe('ViewDriverDeliveryRequestWidgetComponent', () => {
  let component: ViewDriverDeliveryRequestWidgetComponent;
  let fixture: ComponentFixture<ViewDriverDeliveryRequestWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewDriverDeliveryRequestWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewDriverDeliveryRequestWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
