import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-view-driver-delivery-request-widget',
  templateUrl: './view-driver-delivery-request-widget.component.html',
  styleUrls: ['./view-driver-delivery-request-widget.component.scss']
})
export class ViewDriverDeliveryRequestWidgetComponent implements OnInit {
  @Input() delivery: any;
  constructor() { }

  ngOnInit() {
  }

}
