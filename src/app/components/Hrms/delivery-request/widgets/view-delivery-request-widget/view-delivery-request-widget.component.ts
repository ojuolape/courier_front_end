import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-view-delivery-request-widget',
  templateUrl: './view-delivery-request-widget.component.html',
  styleUrls: ['./view-delivery-request-widget.component.scss']
})
export class ViewDeliveryRequestWidgetComponent implements OnInit {

  @Input() delivery: any;

  constructor() { }

  ngOnInit() {
   
  }

  goToPayment(){
    window.location.href = "/payment?amount="+this.delivery.amount/100.00+"&transactionRef="+this.delivery.paymentTrxRef+"&trackingId="+this.delivery.trackingId+"&phone="+this.delivery.customerPhoneNo;
    return;
  }
}
