import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewDeliveryRequestWidgetComponent } from './view-delivery-request-widget.component';

describe('ViewDeliveryRequestWidgetComponent', () => {
  let component: ViewDeliveryRequestWidgetComponent;
  let fixture: ComponentFixture<ViewDeliveryRequestWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewDeliveryRequestWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewDeliveryRequestWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
