import {Component, Input, OnInit} from '@angular/core';
import {ConstNameService} from '../../../../services/const-name.service';
import {QueryResults} from "../../../model/query-result";
import {AppService} from "../../../../services/app.service";
import {HttpErrorResponse} from "@angular/common/http";
import {ToastrService} from "ngx-toastr";
import {SearchService} from "../../../../services/search.service";
declare var jquery: any;
declare var $: any;
declare var require: any;
@Component({
  selector: 'app-delivery-request-search',
  templateUrl: './delivery-request-search.component.html',
  styleUrls: ['./delivery-request-search.component.scss']
})
export class DeliveryRequestSearchComponent implements OnInit {

  itemsPerPage = 10;
  currentPage = 1;
  working = false;
  registering = false;
  queryResult: QueryResults<any>;
  searchFilter :string = "";
  paymentStatus :string = "";
  deliveryStatus :string = "";
  url: string;
  @Input() navTab: string = 'Employee-list';
  constructor(
    private constName: ConstNameService,
    private appService: AppService,
    private searchService: SearchService,
    private toastService: ToastrService
  ) { }

  ngOnInit() {
    this.url = this.constName.baseImage.file_img_url;
    $( document ).ready(function() {
      const DIV_CARD = 'div.card';
      $('[data-toggle="card-remove"]').on('click', function(e) {
        const card = $(this).closest(DIV_CARD);
        card.remove();
        e.preventDefault();
        return false;
      });
      /** Function for collapse card */
      $('[data-toggle="card-collapse"]').on('click', function(e) {
        const card = $(this).closest(DIV_CARD);
        if (card.hasClass('card-collapsed')) {
          card.removeClass('card-collapsed');
        } else {
          card.addClass('card-collapsed');
        }
        e.preventDefault();
        return false;
      });
    });
    this.onPageChanged({page: 1});
  }

  changeNavTab(tab: string) {
    this.navTab = tab;
  }

  get totalCount() {
    return this.queryResult ? this.queryResult.total : 0;
  }

  get offset() {
    return this.queryResult ? ((this.currentPage - 1) * this.itemsPerPage) : 0;
  }

  onSubmit() {
    this.onPageChanged({page: 1});
  }

  onPageChanged(event: { page: number }) {
    this.currentPage = event.page;
    const offset = (event.page - 1) * this.itemsPerPage;
    this.working = true;
    const params = {limit: this.itemsPerPage, pageNumber: (this.currentPage - 1),
       q: this.searchFilter, deliveryStatus: this.deliveryStatus, paymentStatus:this.paymentStatus}
    this.searchService.searchDeliveries( params  )
      .subscribe(result => {
        console.log(result);
        this.working = false;
        this.queryResult = result;

      }, (err: HttpErrorResponse) => {
        this.working = false;
        this.toastService.error('Unable to search at the moment due to an error!');
      });
  }

}
