import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewDeliveryRequestComponent } from './new-delivery-request.component';

describe('NewDeliveryRequestComponent', () => {
  let component: NewDeliveryRequestComponent;
  let fixture: ComponentFixture<NewDeliveryRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewDeliveryRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewDeliveryRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
