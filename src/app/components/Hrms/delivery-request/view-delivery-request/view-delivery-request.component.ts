import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import { map } from 'rxjs/operators';
import {AppService} from "../../../../services/app.service";
import {ConstNameService} from '../../../../services/const-name.service';

import {SearchService} from "../../../../services/search.service";
import {ToastrService} from "ngx-toastr";
import {TokenStorageService} from "../../../Authentication/services/token-storage.service";

@Component({
  selector: 'app-view-delivery-request',
  templateUrl: './view-delivery-request.component.html',
  styleUrls: ['./view-delivery-request.component.scss']
})
export class ViewDeliveryRequestComponent implements OnInit {


  url: string;
  delivery: any;
paymentRef: any;
  firstRole: any;
  driverAssignedDate: Date;
  inTransitDate: Date;
  deliveredDate: Date;
  constructor(private router: ActivatedRoute,
              private constName: ConstNameService,
              private appService: AppService,
              private tokenStorageService: TokenStorageService,
              private toastService: ToastrService) { }

  ngOnInit() {
    this.url = this.constName.baseImage.file_img_url;
    this.router.params
      .pipe(map(params => params['trackingId']))
      .subscribe(trackingId => {

          this.getDeliveryInfo(trackingId);
      });
      
    this.firstRole = this.tokenStorageService.getAuthorities()[0];
  }



  getDeliveryInfo(trackingId) {
    this.appService.getDeliveryRequestByTrackingId(trackingId).subscribe(result => {
      this.delivery = result;
      this.driverAssignedDate = this.getDeliveryHistoryTime('DRIVER_ASSIGNED');
      this.inTransitDate = this.getDeliveryHistoryTime('IN_TRANSIT');
      this.deliveredDate = this.getDeliveryHistoryTime('DELIVERED')
    }, error => {
      this.toastService.error("Delivery request with tracking ID "+trackingId+" does not exist.");
  });
  }


  getDeliveryHistoryTime(status){
    if(!this.delivery){
      return null;
    }
    const date = this.delivery.deliveryHistory.filter(x => x.status === status).map(x => x.dateCreated);
    return date && date != '' ? date : null;
    
  }
}
