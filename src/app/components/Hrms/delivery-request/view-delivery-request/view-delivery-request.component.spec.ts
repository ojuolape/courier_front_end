import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewDeliveryRequestComponent } from './view-delivery-request.component';

describe('ViewDeliveryRequestComponent', () => {
  let component: ViewDeliveryRequestComponent;
  let fixture: ComponentFixture<ViewDeliveryRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewDeliveryRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewDeliveryRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
