import {Component, OnInit, ViewChild} from '@angular/core';
import {ConstNameService} from '../../../../services/const-name.service';
import {GooglePlaceDirective} from "ngx-google-places-autocomplete";
import {Address} from "ngx-google-places-autocomplete/objects/address";
import {SearchService} from "../../../../services/search.service";
import {NgForm} from "@angular/forms";
import {ToastrService} from "ngx-toastr";
import {AppService} from "../../../../services/app.service";
declare var jquery: any;
declare var $: any;
declare var require: any;
@Component({
  selector: 'app-new-admin-delivery-request',
  templateUrl: './new-admin-delivery-request.component.html',
  styleUrls: ['./new-admin-delivery-request.component.scss']
})
export class NewAdminDeliveryRequestComponent implements OnInit {

  pickUpAddress: Address;
  destAddress: Address;
  distance: string;
  trackingId: string;
  registering = false;
  processingEstimate = false;
  form: any = {deliveryAddress:{},pickUpAddress:{}};
  gpsDistance: GpsDistance;
  url: string;
  showDeliveryRequestForm = false;
  @ViewChild("placesRef", {static: true}) placesRef : GooglePlaceDirective;

  public handlePickUpAddressChange(address: Address) {
    this.pickUpAddress = address;
    this.form.pickUpAddress.streetAddress =  address.name + ", " + address.formatted_address;
    // this.getDistanceAndComputeAmountEst();

  }
  public handleDestAddressChange(address: Address) {
    this.destAddress = address;
    this.form.deliveryAddress.streetAddress =  address.name + ", " + address.formatted_address;
   // this.getDistanceAndComputeAmountEst();

  }
  constructor(private constName: ConstNameService,
              private searchService: SearchService,
              private toastService: ToastrService,
              private appService: AppService) { }

  ngOnInit() {
    this.url = this.constName.baseImage.file_img_url;

    $( document ).ready(function() {
      const DIV_CARD = 'div.card';
      $('[data-toggle="card-remove"]').on('click', function(e) {
        const card = $(this).closest(DIV_CARD);
        card.remove();
        e.preventDefault();
        return false;
      });
    });
  }

  toggleCard(){
    if(!( this.form.pickUpAddress.streetAddress &&  this.form.deliveryAddress.streetAddress )){
      return;
    }
    this.getDistanceAndComputeAmountEst();
}
getDistanceAndComputeAmountEst(){
  if(this.processingEstimate){
    return;
  }
  this.showDeliveryRequestForm = false;
  if(this.form.pickUpAddress.streetAddress && this.form.deliveryAddress.streetAddress){
    this.processingEstimate = true;
    const origin = this.form.pickUpAddress.streetAddress;
    const dest = this.form.deliveryAddress.streetAddress;

    this.searchService.getGpsDistance(origin, dest).subscribe(result => {
      this.gpsDistance = result;
      this.processingEstimate = false;
      this.showDeliveryRequestForm = true;
    }, error => {
      this.toastService.error("Could not fetch location, please try another location");
      this.processingEstimate = false;
      this.gpsDistance = null;
      this.showDeliveryRequestForm = false;
    });
   
  }
}

  showLocationForm(){
    this.showDeliveryRequestForm = false;
  }

  createRequest(ngForm: NgForm) {
    if(this.registering){
      return;
    }
    this.trackingId = '';
    if(ngForm.form.invalid || !this.form.pickUpAddress.streetAddress || !this.form.deliveryAddress.streetAddress){
      ngForm.form.markAllAsTouched();
      this.toastService.error('Fill all required fields');
      return;
    }

    if(this.pickUpAddress != null) {
      this.form.pickUpAddress.latitude = this.pickUpAddress.geometry.location.lat();
      this.form.pickUpAddress.longitude = this.pickUpAddress.geometry.location.lng();
    }
    if(this.destAddress != null) {
      this.form.deliveryAddress.latitude = this.destAddress.geometry.location.lat();
      this.form.deliveryAddress.longitude = this.destAddress.geometry.location.lng();
    }
    this.form.estimatedAmount = this.gpsDistance.amount;
    this.registering = true;
    this.appService.createDeliveryRequest(this.form).subscribe(
      data => {
        this.registering = false;
        this.trackingId = data.trackingId ;
        this.resetForm(ngForm);
        //ngForm.reset();
      },
      error => {
        this.registering = false;

        const errorMessage = error.error.errors ? error.error.errors[0].defaultMessage
          : ( (error.error.message && error.error.status != 500) ? error.error.message :'Could not process request, try again later');
        this.toastService.error(errorMessage);
      }
    );
  }

  private resetForm(ngForm: NgForm) {
    this.pickUpAddress = null;
    this.destAddress = null;
    this.form = {deliveryAddress:{},pickUpAddress:{}};
    this.gpsDistance = null;
    this.showDeliveryRequestForm = false;
    ngForm.form.reset();
    ngForm.form.markAsPristine();
  }
}

export class GpsDistance{
  public distance: string;
  public amount: number;

}
