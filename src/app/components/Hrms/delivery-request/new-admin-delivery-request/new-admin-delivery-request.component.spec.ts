import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewAdminDeliveryRequestComponent } from './new-admin-delivery-request.component';

describe('NewAdminDeliveryRequestComponent', () => {
  let component: NewAdminDeliveryRequestComponent;
  let fixture: ComponentFixture<NewAdminDeliveryRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewAdminDeliveryRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewAdminDeliveryRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
