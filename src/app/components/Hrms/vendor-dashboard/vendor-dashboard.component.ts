import { Component, OnInit } from '@angular/core';
import {SearchService} from "../../../services/search.service";
import {QueryResults} from "../../model/query-result";
import {HttpErrorResponse} from "@angular/common/http";
import {AppService} from "../../../services/app.service";
import {TokenStorageService} from "../../Authentication/services/token-storage.service";
@Component({
  selector: 'app-vendor-dashboard',
  templateUrl: './vendor-dashboard.component.html',
  styleUrls: ['./vendor-dashboard.component.scss']
})
export class VendorDashboardComponent implements OnInit {

  working = false;
  queryResult: QueryResults<any>;
  stats: any = {};
  displayName: string;
  constructor(
    private searchService: SearchService,
    private appService: AppService,
    private storageService: TokenStorageService
  ) {
  }

  ngOnInit() {
    this.fetchLatestDeliveries();
    this.fetchDeliveriesStats();
    this.displayName = this.storageService.getDisplayName();
  }

  fetchLatestDeliveries(){
    this.working = false;
    const params = {limit: 5, pageNumber: 0}
    this.searchService.searchDeliveries(params )
      .subscribe(result => {
        this.working = false;
        this.queryResult = result;

      }, (err: HttpErrorResponse) => {
        this.working = false;
      });
  }

  fetchDeliveriesStats(){
    this.appService.getAdminDashboardStats().subscribe(response => {
      this.stats = response;
    })
  }

}
