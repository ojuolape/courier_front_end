import { Component, OnInit, ViewChild } from '@angular/core';
import {GooglePlaceDirective} from "ngx-google-places-autocomplete";
import {Address} from "ngx-google-places-autocomplete/objects/address";
import {SearchService} from "../../services/search.service";
import {NgForm} from "@angular/forms";
import {ToastrService} from "ngx-toastr";
import {AppService} from "../../services/app.service";
declare var jquery: any;
declare var $: any;
declare var require: any;
declare const fbq: any; 

@Component({
  selector: 'app-website',
  templateUrl: './website.component.html',
  styleUrls: ['./website.component.scss']
})
export class WebsiteComponent implements OnInit {
  pickUpAddress: Address;
  destAddress: Address;
  distance: string;
  trackingId: string;
  registering = false;
  processingEstimate = false;
  form: any = {deliveryAddress:{},pickUpAddress:{}, deliveryType:""};
  gpsDistance: GpsDistance;
  url: string;
  @ViewChild("placesRef", {static: true}) placesRef : GooglePlaceDirective;

  public handlePickUpAddressChange(address: Address) {
    this.pickUpAddress = address;
    this.form.pickUpAddress.streetAddress = address.formatted_address;
    // this.getDistanceAndComputeAmountEst();

  }
  public handleDestAddressChange(address: Address) {
    this.destAddress = address;
    this.form.deliveryAddress.streetAddress = address.formatted_address;
    //this.getDistanceAndComputeAmountEst();

  }
  constructor(private searchService: SearchService,
              private toastService: ToastrService,
              private appService: AppService) { }

  ngOnInit() {
    $( document ).ready(function() {
      $('#proceedButton').on('click', function(e) {
        //alert('yay');
        fbq('track', 'CompleteRegistration');
        //return false;
      });
    });
  }


  getDistanceAndComputeAmountEst(){
    if(this.processingEstimate){
      return;
    }
    if(this.pickUpAddress && this.destAddress){
      this.processingEstimate = true;
      const origin = this.pickUpAddress.geometry.location.lat()+','+this.pickUpAddress.geometry.location.lng();
      const dest = this.destAddress.geometry.location.lat()+','+this.destAddress.geometry.location.lng();

      this.searchService.getGpsDistance(origin, dest).subscribe(result => {
        //this.gpsDistance2 = result;
        console.log(result);
        this.gpsDistance = result;
      });
      this.processingEstimate = false;
    }
  }


  createRequest(ngForm: NgForm) {
    if(this.registering){
      return;
    }
    this.trackingId = '';
    if(ngForm.form.invalid || !this.form.pickUpAddress.streetAddress || !this.form.deliveryAddress.streetAddress){
      ngForm.form.markAllAsTouched();
      this.toastService.error('Fill all required fields');
      return;
    }

    if(this.pickUpAddress != null) {
      this.form.pickUpAddress.latitude = this.pickUpAddress.geometry.location.lat();
      this.form.pickUpAddress.longitude = this.pickUpAddress.geometry.location.lng();
    }
    if(this.destAddress != null) {
      this.form.deliveryAddress.latitude = this.destAddress.geometry.location.lat();
      this.form.deliveryAddress.longitude = this.destAddress.geometry.location.lng();
    }

    
    this.appService.createDeliveryRequestExtranet(this.form).subscribe(
      data => {
        this.registering = false;
        this.trackingId = data.trackingId ;
        this.resetForm(ngForm);
        //ngForm.reset();


      },
      error => {
        this.registering = false;

        const errorMessage = error.error.errors ? error.error.errors[0].defaultMessage
          : ( (error.error.message && error.error.status != 500) ? error.error.message :'Could not process request, try again later');
        this.toastService.error(errorMessage);
      }
    );
  }

  private resetForm(ngForm: NgForm) {
    this.pickUpAddress = null;
    this.destAddress = null;
    this.form = {deliveryAddress:{},pickUpAddress:{}, deliveryType:""};
    ngForm.form.reset();
    ngForm.form.markAsPristine();
  }
}

export class GpsDistance{
  public distance: string;
  public amount: number;

}

