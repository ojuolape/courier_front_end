import { Component, OnInit, ViewChild } from '@angular/core';
import {GooglePlaceDirective} from "ngx-google-places-autocomplete";
import {Address} from "ngx-google-places-autocomplete/objects/address";
import {SearchService} from "../../services/search.service";
import {NgForm} from "@angular/forms";
import {ToastrService} from "ngx-toastr";
import {AppService} from "../../services/app.service";
declare var jquery: any;
declare var $: any;
declare var require: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {
  keyword = 'type';
  public item = [
    {
      id: 1,
      type: 'Food',
    },
    {
      id: 2,
      type: 'Human hair',
    },
    {
      id: 3,
      type: 'Perfume and oils',
    },
    {
      id: 4,
      type: 'Shoes',
    },
    {
      id: 5,
      type: 'Fragrance',
    },
    {
      id: 6,
      type: 'Skincare',
    },
    {
      id: 7,
      type: 'Makeup',
    },
    {
      id: 8,
      type: 'Slovenia',
    },
    {
      id: 9,
      type: 'Cream and lotion',
    },
    {
      id: 10,
      type: 'Makeup and beauty',
    },
    {
      id: 11,
      type: 'Bath and shower',
    },
    {
      id: 12,
      type: 'Document',
    },
    {
      id: 13,
      type: 'Mucical instrument',
    },
    {
      id: 14,
      type: 'Magazines',
    },
    {
      id: 15,
      type: 'Camera',
    },
    {
      id: 16,
      type: 'Mobile phone',
    },
    {
      id: 17,
      type: 'Gadgets',
    },
    {
      id: 18,
      type: 'Electronics',
    },
    {
      id: 19,
      type: 'Jewelry',
    },
    {
      id: 20,
      type: 'Fashion',
    },
    {
      id: 21,
      type: 'Clothes',
    },
    {
      id: 22,
      type: 'Photographs',
    },
    {
      id: 23,
      type: 'Accessories',
    },
    {
      id: 24,
      type: 'Snacks',
    },
    {
      id: 25,
      type: 'Grocery',
    },
    {
      id: 26,
      type: 'Meat',
    },
    {
      id: 27,
      type: 'Gifts',
    },
    {
      id: 28,
      type: 'Drugs',
    },
    {
      id: 29,
      type: 'Clothes',
    },
    {
      id: 30,
      type: 'Bags',
    },
    {
      id: 31,
      type: 'Books',
    },
    {
      id: 32,
      type: 'Glasses',
    },
    {
      id: 33,
      type: 'Toys',
    },
    {
      id: 34,
      type: 'Drinks',
    },
    {
      id: 30,
      type: 'Games',
    }
  ];

  pickUpAddress: Address;
  destAddress: Address;
  distance: string;
  trackingId: string;
  registering = false;
  redirecting = false;
  processingEstimate = false;
  form: any = {deliveryAddress:{},pickUpAddress:{}, deliveryType:"EXPRESS"};
  gpsDistance: GpsDistance;
  url: string;
  showDeliveryRequestForm = false;
  searching = false;
  searchMessage = "";
  searchTrackingId = "";
  trackedDelivery: any;
  @ViewChild("placesRef", {static: true}) placesRef : GooglePlaceDirective;
  

  public handlePickUpAddressChange(address: Address) {
    this.pickUpAddress = address;
    this.form.pickUpAddress.streetAddress = address.name + ", " + address.formatted_address;
    // this.getDistanceAndComputeAmountEst();

  }
  public handleDestAddressChange(address: Address) {
    this.destAddress = address;
    this.form.deliveryAddress.streetAddress = address.name + ", " + address.formatted_address;
    //this.getDistanceAndComputeAmountEst();

  }
  constructor(private searchService: SearchService,
              private toastService: ToastrService,
              private appService: AppService) { }


  ngOnInit() {
  }

  getDistanceAndComputeAmountEst(){
    if(this.processingEstimate){
      return;
    }
    if(this.form.pickUpAddress.streetAddress && this.form.deliveryAddress.streetAddress){
      this.processingEstimate = true;
      const origin = this.form.pickUpAddress.streetAddress;
      const dest = this.form.deliveryAddress.streetAddress;

      this.searchService.getGpsDistance(origin, dest).subscribe(result => {
        this.gpsDistance = result;
        this.processingEstimate = false;
      }, error => {
        this.toastService.error("Could not fetch location, please try another location");
        this.processingEstimate = false;
        this.gpsDistance = null;
      });
     
    }
  }

  toggleCard(){
    if(!this.showDeliveryRequestForm){
      if(!( this.form.pickUpAddress.streetAddress &&  this.form.deliveryAddress.streetAddress )){
        return;
      }
      this.getDistanceAndComputeAmountEst();
    }
    this.showDeliveryRequestForm = !this.showDeliveryRequestForm;
  }
  selectEvent (item) {
    // do something with selected item
    if(item){
      this.form.parcelDetails = item.type;
    }
    this.form.parcelDetails = "";
  }

  onChangeSearch(search: string) {
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
  }

  onFocused (e) {
    // do something
  }

  createRequest(ngForm: NgForm) {
    if(this.registering){
      return;
    }
    this.trackingId = '';
    if(ngForm.form.invalid || !this.form.pickUpAddress.streetAddress || !this.form.deliveryAddress.streetAddress){
      ngForm.form.markAllAsTouched();
      this.toastService.error('Fill all required fields');
      return;
    }

    if(this.pickUpAddress != null) {
      this.form.pickUpAddress.latitude = this.pickUpAddress.geometry.location.lat();
      this.form.pickUpAddress.longitude = this.pickUpAddress.geometry.location.lng();
    }
    if(this.destAddress != null) {
      this.form.deliveryAddress.latitude = this.destAddress.geometry.location.lat();
      this.form.deliveryAddress.longitude = this.destAddress.geometry.location.lng();
    }
    this.form.estimatedAmount = this.gpsDistance.amount;
    
    this.appService.createDeliveryRequestExtranet(this.form).subscribe(
      data => {
        this.registering = false;
        this.trackingId = data.trackingId ;
        const tr = data.paymentRef;
        const phone = this.form.customerPhoneNo;
        this.redirecting = true;
        window.location.href = `/payment?amount=${this.gpsDistance.amount/100.00}&transactionRef=${tr}&trackingId=${data.trackingId}&phone=${phone}`;
 


      },
      error => {
        this.registering = false;

        const errorMessage = error.error.errors ? error.error.errors[0].defaultMessage
          : ( (error.error.message && error.error.status != 500) ? error.error.message :'Could not process request, try again later');
        this.toastService.error(errorMessage);
      }
    );
  }

  private resetForm(ngForm: NgForm) {
    this.pickUpAddress = null;
    this.destAddress = null;
    this.form = {deliveryAddress:{},pickUpAddress:{}, deliveryType:""};
    ngForm.form.reset();
    ngForm.form.markAsPristine();
  }

  fetchDeliveryInfoByTrackingId(){
    console.log(this.searchTrackingId)
    this.searchMessage = '';
    if(!this.searchTrackingId){
      this.searchMessage = "Please enter Tracking ID";
      return;
    }

    this.searching = true;
    this.appService.getDeliveryRequestByTrackingId(this.searchTrackingId).subscribe(result => {
      this.trackedDelivery = result;
      this.searching = false;
    }, error => {
      this.searching = false;
      this.searchMessage = "Delivery request with Tracking ID '"+this.searchTrackingId+"' does not exist.";
  });

  }
}

export class GpsDistance{
  public distance: string;
  public amount: number;
  public duration: number;
  public time: string;

}


