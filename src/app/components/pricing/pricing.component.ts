import { Component, OnInit, ViewChild } from '@angular/core';
import {GooglePlaceDirective} from "ngx-google-places-autocomplete";
import {Address} from "ngx-google-places-autocomplete/objects/address";
import {SearchService} from "../../services/search.service";
import {NgForm} from "@angular/forms";
import {ToastrService} from "ngx-toastr";
import {AppService} from "../../services/app.service";
@Component({
  selector: 'app-pricing',
  templateUrl: './pricing.component.html',
  styleUrls: ['./pricing.component.scss']
})
export class PricingComponent implements OnInit {

  pickUpAddress: Address;
  destAddress: Address;
  distance: string;
  processingEstimate = false;
  form: any = {deliveryAddress:{},pickUpAddress:{}, deliveryType:"EXPRESS"};
  gpsDistance: GpsDistance;
  url: string;
  showDeliveryRequestForm = false;
  showEstimateTable = false;
 
  @ViewChild("placesRef", {static: true}) placesRef : GooglePlaceDirective;

  public handlePickUpAddressChange(address: Address) {
    this.pickUpAddress = address;
    this.form.pickUpAddress.streetAddress = address.name + ", " + address.formatted_address;
    // this.getDistanceAndComputeAmountEst();

  }
  public handleDestAddressChange(address: Address) {
    this.destAddress = address;
    this.form.deliveryAddress.streetAddress = address.name + ", " + address.formatted_address;
    //this.getDistanceAndComputeAmountEst();

  }
  
  old() {
    var t = document.getElementById("estimateTable");
    // var c = document.getElementById("closeEstimateTable");
    console.log(t.style.display)
    if (t.style.display === "none") {
        t.style.display = "block";
      }
      return;
  }

 
  constructor(private searchService: SearchService, private toastService: ToastrService) { }

  ngOnInit() {
  }

  estimateTableToggle(){
    if( !(this.form.pickUpAddress.streetAddress && this.form.deliveryAddress.streetAddress)){
        return;
    }
    this.getDistanceAndComputeAmountEst();
    
  }

    closeTable() {
      this.showEstimateTable = false;
    }

    getDistanceAndComputeAmountEst(){
    if(this.processingEstimate){
      return;
    }
    this.showEstimateTable = false;
    if(this.form.pickUpAddress.streetAddress && this.form.deliveryAddress.streetAddress){
      this.processingEstimate = true;
      const origin = this.form.pickUpAddress.streetAddress;
      const dest = this.form.deliveryAddress.streetAddress;

      this.searchService.getGpsDistance(origin, dest).subscribe(result => {
        this.gpsDistance = result;
        this.processingEstimate = false;
        this.showEstimateTable = true;
      }, error => {
        this.toastService.error("Could not fetch location, please try another location");
        this.processingEstimate = false;
        this.gpsDistance = null;
        this.showEstimateTable = false;
      });
     
    }
  }

}
export class GpsDistance{
  public distance: string;
  public amount: number;
  public duration: number;
  public time: string;

}


