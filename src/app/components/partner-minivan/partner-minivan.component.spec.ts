import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnerMinivanComponent } from './partner-minivan.component';

describe('PartnerMinivanComponent', () => {
  let component: PartnerMinivanComponent;
  let fixture: ComponentFixture<PartnerMinivanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartnerMinivanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerMinivanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
