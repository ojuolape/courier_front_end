import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { SignUpInfo } from '../services/signup-info';
import {ToastrService} from "ngx-toastr";
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  form: any = {};
  signupInfo: SignUpInfo;
  errorMessage = '';
  successMessage = '';

  constructor(private authService: AuthService,
              private toastr: ToastrService) { }

  ngOnInit() { }

  onSubmit() {

    this.successMessage = '';
    this.errorMessage = '';

    this.signupInfo = new SignUpInfo(
      this.form.name,
      this.form.email,
      this.form.email,
      this.form.password,
      this.form.phoneNumber);

    this.authService.signUp(this.signupInfo).subscribe(
      data => {
        this.successMessage = 'Your registration is successful. Please login!';
        this.form = {};
      },
      error => {
        this.errorMessage = error.error.errors ? error.error.errors[0].defaultMessage : ( error.error.message ? error.error.message :'Could not process request, try again later');
      }
    );
  }
}
