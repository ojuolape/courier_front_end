import { Component, OnInit } from '@angular/core';
import {NgForm} from "@angular/forms";
import {AuthService} from "../services/auth.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-update-password',
  templateUrl: './update-password.component.html',
  styleUrls: ['./update-password.component.scss']
})
export class UpdatePasswordComponent implements OnInit {

  form: any = {};
  isLoading = false;
  isValidating = false;
  errorMessage = '';
  successMessage = '';
  isValidated = false;
  token: string;
  constructor(private authService: AuthService, private router: ActivatedRoute,) { }

  ngOnInit() {
    this.router.queryParams.subscribe(res => {
      this.token = res['pwt'];
      this.validateToken();
    });

  }

  validateToken(){
    this.isValidating = true;
    this.authService.validateResetPasswordToken(this.token).subscribe(
      data => {
        this.isValidating = false;
        if(data['message'] === 'valid'){
          this.isValidated = true;
        } else {
          this.errorMessage = 'Reset link has expired';
        }


      },
      res => {
        this.isValidating = false;
        if (res.status === 400) {
          this.errorMessage = 'Invalid reset link';
        } else if (res.status < 1) {
          this.errorMessage = 'Failed to contact server. Please check your internet connection';
        } else {
          this.errorMessage = 'An error occurred. Please try again';
        }

      }
    );
  }
  updatePassword(ngForm: NgForm){

    if(this.isLoading){
      return;
    }
    this.errorMessage = '';
    this.successMessage = '';
    if(ngForm.form.invalid){
      ngForm.form.markAllAsTouched();
      this.errorMessage = ('Fill all required fields');
      return;
    }

    if(this.form.password !== this.form.confirmPassword){
      return;
    }
    this.isLoading = true;
    const param = {password: this.form.password,
    token: this.token};

    this.authService.updatePassword(param).subscribe(
      data => {
        this.isLoading = false;
        this.successMessage = ('Reset successful');
        //ngForm.reset();
        ngForm.form.reset();
        ngForm.form.markAsPristine();

      },
      res => {
        this.isLoading = false;
        if (res.status === 400) {
          this.errorMessage = 'Unable to reset password.';
        } else if (res.status < 1) {
          this.errorMessage = 'Failed to contact server. Please check your internet connection';
        } else {
          this.errorMessage = 'An error occurred. Please try again';
        }

      }
    );
  }
}
