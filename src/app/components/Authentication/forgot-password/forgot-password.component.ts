import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {AuthService} from "../services/auth.service";

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

  form: any = {};
  isLoading = false;
  errorMessage = '';
  successMessage = '';

  constructor(private authService: AuthService) { }

  ngOnInit() {
  }

  onSubmit() {
    if(this.isLoading){
      return;
    }
    this.successMessage = '';
    this.errorMessage = '';
    this.isLoading = true;
    const param = {email: this.form.email};

    this.authService.resetPassword(param).subscribe(
      data => {
        this.isLoading = false;
        this.successMessage = "Success";
        this.form = {};
      },
      res => {

        this.isLoading = false;
        if (res.status === 400) {
          this.errorMessage = 'User with email address does not exist';
        } else if (res.status < 1) {
          this.errorMessage = 'Failed to contact server. Please check your internet connection';
        } else {
          this.errorMessage = 'An error occured. Please try again';
        }
      }
    );
  }
}
