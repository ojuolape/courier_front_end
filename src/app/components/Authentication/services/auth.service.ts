import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subject,Observable } from 'rxjs';
import {environment} from "../../../../environments/environment";
import { of } from 'rxjs';
import { JwtResponse } from './jwt-response';
import { AuthLoginInfo } from './login-info';
import { SignUpInfo } from './signup-info';
import {TokenStorageService} from "./token-storage.service";

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private loginUrl = `${environment.baseUrl}/api/auth/signin`;
  private signupUrl = `${environment.baseUrl}/api/auth/signup`;
  private userLoggedIn : boolean;

  constructor(private http: HttpClient, private tokenStorage: TokenStorageService) {
   // this.userLoggedIn.next(false);
  }

  attemptAuth(credentials: AuthLoginInfo): Observable<JwtResponse> {
    return this.http.post<JwtResponse>(this.loginUrl, credentials, httpOptions);
  }

  signUp(info: SignUpInfo): Observable<string> {
    return this.http.post<string>(this.signupUrl, info, httpOptions);
  }

  setUserLoggedIn(userLoggedIn: boolean) {
    this.userLoggedIn = (userLoggedIn);
  }

  getUserLoggedIn(): Observable<boolean> {
    return of(this.userLoggedIn);
  }

  logout(){
    this.setUserLoggedIn(false);
    this.tokenStorage.signOut();
  }

  resetPassword(param: any): Observable<any> {
    return this.http.post<any>(`${environment.baseUrl}/user/resetPassword`, param, httpOptions);
  }

  updatePassword(param: any): Observable<any> {
    return this.http.post<any>(`${environment.baseUrl}/user/savePassword`, param);
  }

  validateResetPasswordToken(token: string): Observable<any> {
    return this.http.get<any>(`${environment.baseUrl}/user/changePassword?token=${token}`)
  }

  registerFcmToken(token) {
    const data = {
      token: token
    }
    return this.http.post<any>(`${environment.baseUrl}/user/fcm-token`, data, httpOptions).subscribe(data => {
      console.log('token sent')
    });
  }
}
