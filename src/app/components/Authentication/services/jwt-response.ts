export class JwtResponse {
    token: string;
    email: string;
  displayName: string;
    username: string;
    authorities: string[];
}
