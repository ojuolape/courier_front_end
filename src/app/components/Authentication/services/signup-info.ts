export class SignUpInfo {
    name: string;
    username: string;
    email: string;
    role: string[];
    password: string;
    phoneNumber: string;

    constructor(name: string, username: string, email: string, password: string, phoneNumber: string) {
        this.name = name;
        this.username = username;
        this.email = email;
        this.password = password;
        this.phoneNumber = phoneNumber;
        this.role = ['VENDOR'];
    }
}
