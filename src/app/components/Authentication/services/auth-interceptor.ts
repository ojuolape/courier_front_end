import {HTTP_INTERCEPTORS, HttpErrorResponse, HttpEvent} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import * as moment from 'moment';
import {environment} from "../../../../environments/environment";

import { TokenStorageService } from './token-storage.service';
import {ToastrService} from "ngx-toastr";
import {Observable, of, throwError} from "rxjs/index";
import {catchError, tap} from "rxjs/internal/operators";
import {AuthService} from "./auth.service";

const TOKEN_HEADER_KEY = 'Authorization';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(private tokenStorageService: TokenStorageService,
                private authService: AuthService) { }


    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>  {
        let authReq = req;
        const token = this.tokenStorageService.getToken();
        if (token != null) {
            authReq = req.clone({ headers: req.headers.set(TOKEN_HEADER_KEY, 'Bearer ' + token) });
        }
        return next.handle(authReq).pipe(
          tap(evt => {}),
          catchError((error:any) => {
            if(error instanceof HttpErrorResponse) {
              try {
                if(error.error.message === 'Expired Token'){
                  this.authService.logout();
                }
              } catch(e) {
              }
              //log error
            }
            return throwError(error);

          }));
    }
}

export const httpInterceptorProviders = [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
];
