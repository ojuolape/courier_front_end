import { Component, OnInit } from '@angular/core';
import {AuthLoginInfo} from "../services/login-info";
import {AuthService} from "../services/auth.service";
import {TokenStorageService} from "../services/token-storage.service";
import {Router} from "@angular/router";
import { MessagingService } from 'src/app/services/messaging.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: any = {};
  isLoading = false;
  errorMessage = '';
  roles: string[] = [];
  private loginInfo: AuthLoginInfo;

  constructor(private authService: AuthService,
              private router: Router, private messagingService: MessagingService,
              private tokenStorage: TokenStorageService) { }

  ngOnInit() {
    if (this.tokenStorage.getToken()) {
      this.roles = this.tokenStorage.getAuthorities();
      //this.reloadPage();
    }
  }

  onSubmit() {
    if(this.isLoading){
      return;
    }
    this.isLoading = true;
    this.loginInfo = new AuthLoginInfo(
      this.form.username,
      this.form.password);

    this.authService.attemptAuth(this.loginInfo).subscribe(
      data => {

        this.tokenStorage.saveToken(data.token);
        this.tokenStorage.saveUsername(data.username);
        this.tokenStorage.saveAuthorities(data.authorities);
        this.tokenStorage.saveDisplayName(data.displayName);
        this.tokenStorage.saveEmail(data.email);
        this.isLoading = false;
        this.authService.setUserLoggedIn(true);
       
        this.router.navigate(['/dashboard']);



      },
      res => {

        this.isLoading = false;
        if (res.status === 401 || res.status === 400) {
          this.errorMessage = 'Username or Password incorrect';
        } else if (res.status < 1) {
          this.errorMessage = 'Failed to contact server. Please check your internet connection';
        } else {
          this.errorMessage = 'An error occured. Please try again';
        }
      }
    );
  }


  containsRole(roles: string[], roleName: string){
    if (roles.indexOf(roleName) > -1) {
      return true;
    } else {
      return false;
    }
  }

}
