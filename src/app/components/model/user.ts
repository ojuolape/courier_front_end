export class UserModel{
  public name: string;
  public dateCreated: Date;
  public roles: string[];
  public userName: string;
  public phoneNumber: string;
  public id: number;
}
