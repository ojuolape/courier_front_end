export class DriverModel{
  public name: string;
  public dateCreated: Date;
  public emailAddress: string;
  public phoneNumber: string;
  public id: number;
}
