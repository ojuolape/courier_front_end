import {UserModel} from "./user";
import {DriverModel} from "./driver";

export class VehicleModel{
  public vehicleNumber: string;
  public dateCreated: Date;
  public createdBy: UserModel;
  public driver: DriverModel;
  public id: number;
}
