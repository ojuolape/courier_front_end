import { NotificationModel } from './notification';

export class FcmNotificationModel{
  public notification: NotificationModel;
  public data: any;
}
