export class RegisterUser {
  name: string;
  username: string;
  email: string;
  role: string[];
  password: string;
  phoneNumber: string;

  constructor(name: string, role: string[], email: string, password: string, phoneNumber: string) {
    this.name = name;
    this.username = email;
    this.email = email;
    this.password = password;
    this.phoneNumber = phoneNumber;
    this.role = role;
  }
}
