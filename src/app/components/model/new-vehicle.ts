export class RegisterVehicle {
  vehicleNumber: string;
  vehicleType: string;


  constructor(vehicleNumber: string, vehicleType: string) {
    this.vehicleType = vehicleType;
    this.vehicleNumber = vehicleNumber;
  }
}
