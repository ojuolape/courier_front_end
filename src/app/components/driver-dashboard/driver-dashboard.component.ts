/// <reference types="@types/googlemaps" />

import { Component, OnInit } from '@angular/core';
import {SearchService} from "../../services/search.service";
import {QueryResults} from "../model/query-result";
import {HttpErrorResponse} from "@angular/common/http";
import {AppService} from "../../services/app.service";
import {TokenStorageService} from "../Authentication/services/token-storage.service";
import { VehicleService } from 'src/app/services/vehicle.service';
@Component({
  selector: 'app-driver-dashboard',
  templateUrl: './driver-dashboard.component.html',
  styleUrls: ['./driver-dashboard.component.scss']
})
export class DriverDashboardComponent implements OnInit {

  working = false;
  queryResult: QueryResults<any>;
  stats: any = {};
  displayName: string;
  currentLat: any;
  currentLong: any;
  private geoCoder;
  
  constructor(
    private searchService: SearchService,
    private appService: AppService,
    private storageService: TokenStorageService,
  ) {
  }

  ngOnInit() {
    this.fetchLatestDeliveries();
    this.fetchDeliveriesStats();
    this.displayName = this.storageService.getDisplayName();
  }

  trackMe() {
    if (navigator.geolocation) {
      navigator.geolocation.watchPosition((position) => {
        this.showTrackingPosition(position);
      });
    } else {
      alert("Geolocation is not supported by this browser.");
    }
  }

  showTrackingPosition(position) {
    //console.log(`tracking postion:  ${position.coords.latitude} - ${position.coords.longitude}`);
    this.currentLat = position.coords.latitude;
    this.currentLong = position.coords.longitude;

   // let location = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
   // console.log(location)
  
    const data = {longitude: this.currentLong,
                  latitude: this.currentLat};
    this.appService.sendLocation(data).subscribe(response => {
       // console.log("location sent");
    });

  }

  fetchLatestDeliveries(){
    this.working = false;
    const params = {limit: 5, pageNumber: 0}
    this.searchService.searchDeliveries(params )
      .subscribe(result => {
        this.working = false;
        this.queryResult = result;

      }, (err: HttpErrorResponse) => {
        this.working = false;
      });
  }

  fetchDeliveriesStats(){
    this.appService.getAdminDashboardStats().subscribe(response => {
      this.stats = response;
    })
  }

  getAddress(latitude, longitude) {
    this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          return results[0].formatted_address;
        } 
        return "";
      } 
      return "";
 
    });
  }
}
