import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { FullCalendarModule } from '@fullcalendar/angular';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import {NgApexchartsModule} from 'ng-apexcharts';
import { AgmCoreModule } from '@agm/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/Authentication/login/login.component';
import { HeaderTopComponent } from './shared-module/navbar/header-top/header-top.component';
import { LeftSidebarComponent } from './shared-module/navbar/left-sidebar/left-sidebar.component';
import { PageTopbarComponent } from './shared-module/navbar/page-topbar/page-topbar.component';
import { DashboardComponent } from './components/Hrms/dashboard/dashboard.component';
import { FooterComponent } from './components/footer/footer.component';
import {RichTextEditorAllModule} from '@syncfusion/ej2-angular-richtexteditor';
import { UserComponent } from './components/Hrms/user/user.component';
import { DepartmentsComponent } from './components/Hrms/departments/departments.component';
import { EmployeeComponent } from './components/Hrms/employee/employee.component';
import { ActivitiesComponent } from './components/Hrms/activities/activities.component';
import { HolidayComponent } from './components/Hrms/holiday/holiday.component';
import { EventsComponent } from './components/Hrms/events/events.component';
import { PayrollComponent } from './components/Hrms/payroll/payroll.component';
import { AccountsComponent } from './components/Hrms/accounts/accounts.component';
import { ReportComponent } from './components/Hrms/report/report.component';
import { ProjectDashboardComponent } from './components/Project/project-dashboard/project-dashboard.component';
import { ProjectListComponent } from './components/Project/project-list/project-list.component';
import { ProjectTaskboardComponent } from './components/Project/project-taskboard/project-taskboard.component';
import { ProjectTicketListComponent } from './components/Project/project-ticket-list/project-ticket-list.component';
import { ProjectTicketDetailComponent } from './components/Project/project-ticket-detail/project-ticket-detail.component';
import { ProjectClientsComponent } from './components/Project/project-clients/project-clients.component';
import { ProjectTodoListComponent } from './components/Project/project-todo-list/project-todo-list.component';
import { JobDashboardComponent } from './components/Jobs/job-dashboard/job-dashboard.component';
import { JobPosotionsComponent } from './components/Jobs/job-posotions/job-posotions.component';
import { JobApplicationComponent } from './components/Jobs/job-application/job-application.component';
import { JobResumesComponent } from './components/Jobs/job-resumes/job-resumes.component';
import { JobSettingsComponent } from './components/Jobs/job-settings/job-settings.component';
import { RegisterComponent } from './components/Authentication/register/register.component';
import { ForgotPasswordComponent } from './components/Authentication/forgot-password/forgot-password.component';
import { PageNotFoundComponent } from './components/Authentication/page-not-found/page-not-found.component';
import { PageNotWorkingComponent } from './components/Authentication/page-not-working/page-not-working.component';
import { FontAwesomeComponent } from './components/Icon/font-awesome/font-awesome.component';
import { FeatherComponent } from './components/Icon/feather/feather.component';
import { LineIconComponent } from './components/Icon/line-icon/line-icon.component';
import { FlagIconComponent } from './components/Icon/flag-icon/flag-icon.component';
import { PaymentIconComponent } from './components/Icon/payment-icon/payment-icon.component';
import { GalleryComponent } from './components/gallery/gallery.component';
import { CardComponent } from './components/Widgets/card/card.component';
import { CardImagesComponent } from './components/Widgets/card-images/card-images.component';
import { DataComponent } from './components/Widgets/data/data.component';
import { SocialComponent } from './components/Widgets/social/social.component';
import { CalendarComponent } from './components/Sidebar/calendar/calendar.component';
import { ChatComponent } from './components/Sidebar/chat/chat.component';
import { ContactInfoComponent } from './components/Sidebar/contact-info/contact-info.component';
import { FileManagerComponent } from './components/Sidebar/file-manager/file-manager.component';
import { PageSearchComponent } from './components/Sidebar/page-search/page-search.component';
import { AppSettingComponent } from './components/Topbar/app-setting/app-setting.component';
import { PageProfileComponent } from './components/Topbar/page-profile/page-profile.component';
import { httpInterceptorProviders } from './components/Authentication/services/auth-interceptor';
import {HttpClientModule} from "@angular/common/http";
import {ToastrModule} from "ngx-toastr";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {ModalModule, PaginationModule} from "ngx-bootstrap";
import { VehiclesComponent } from './components/Hrms/vehicles/vehicles.component';
import {GooglePlaceModule} from "ngx-google-places-autocomplete";
import {DeliveryRequestSearchComponent} from "./components/Hrms/delivery-request/delivery-request-search/delivery-request-search.component";
import {NewDeliveryRequestComponent} from "./components/Hrms/delivery-request/vendor/new-delivery-request/new-delivery-request.component";
import {ViewDeliveryRequestComponent} from "./components/Hrms/delivery-request/view-delivery-request/view-delivery-request.component";
import { AdminDashboardComponent } from './components/Hrms/admin-dashboard/admin-dashboard.component';
import { DefaultDashboardComponent } from './components/Hrms/default-dashboard/default-dashboard.component';
import { VendorDashboardComponent } from './components/Hrms/vendor-dashboard/vendor-dashboard.component';
import { PartnerDashboardComponent } from './components/Hrms/partner-dashboard/partner-dashboard.component';
import {NgIdleKeepaliveModule} from "@ng-idle/keepalive";
import {MomentModule} from "angular2-moment";
import { UpdatePasswordComponent } from './components/Authentication/update-password/update-password.component';
import {LoggedInGuardGuard} from "./components/Authentication/services/guards/logged-in-guard.guard";
import {NewAdminDeliveryRequestComponent} from "./components/Hrms/delivery-request/new-admin-delivery-request/new-admin-delivery-request.component";
import {ViewAdminDeliveryRequestWidgetComponent} from "./components/Hrms/delivery-request/widgets/view-admin-delivery-request-widget/view-admin-delivery-request-widget.component";
import {ViewDeliveryRequestWidgetComponent} from "./components/Hrms/delivery-request/widgets/view-delivery-request-widget/view-delivery-request-widget.component";
import { WebsiteComponent } from './components/website/website.component';
import { DriverDashboardComponent } from './components/driver-dashboard/driver-dashboard.component';
import {ViewDriverDeliveryRequestWidgetComponent} from "./components/Hrms/delivery-request/widgets/view-driver-delivery-request-widget/view-driver-delivery-request-widget.component";
import {DeliveryStatusUpdateWidgetComponent} from "./components/Hrms/delivery-request/widgets/delivery-status-update-widget/delivery-status-update-widget.component";
import { AngularRaveModule } from 'angular-rave';
import { PaymentComponent } from './payment/payment.component';
import { PaymentConfirmationComponent } from './payment-confirmation/payment-confirmation.component';
import { PaymentFailedComponent } from './payment-failed/payment-failed.component';
import { HomepageComponent } from './homepage/homepage.component';
import { HomeComponent } from './components/home/home.component';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';
import { MessagingService } from './services/messaging.service';
import { environment } from '../environments/environment';
import { AsyncPipe } from '../../node_modules/@angular/common';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { WebViewDeliveryRequestComponent } from './components/Hrms/delivery-request/web-view-delivery-request/web-view-delivery-request.component';
import { PricingComponent } from './components/pricing/pricing.component';
import { FaqsComponent } from './components/faqs/faqs.component';
import { TermsComponent } from './components/terms/terms.component';
import { ShippingPolicyComponent } from './components/shipping-policy/shipping-policy.component';
import { PartnerComponent } from './components/partner/partner.component';
import { PartnerMinivanComponent } from './components/partner-minivan/partner-minivan.component';
import { FcmNotificationComponent } from './fcm-notification/fcm-notification.component';
import { DriverProfileComponent } from './components/profiles/driver-profile/driver-profile.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderTopComponent,
    LeftSidebarComponent,
    PageTopbarComponent,
    DashboardComponent,
    FooterComponent,
    UserComponent,
    DepartmentsComponent,
    EmployeeComponent,
    ActivitiesComponent,
    HolidayComponent,
    EventsComponent,
    PayrollComponent,
    AccountsComponent,
    ReportComponent,
    ProjectDashboardComponent,
    ProjectListComponent,
    ProjectTaskboardComponent,
    ProjectTicketListComponent,
    ProjectTicketDetailComponent,
    ProjectClientsComponent,
    ProjectTodoListComponent,
    JobDashboardComponent,
    JobPosotionsComponent,
    JobApplicationComponent,
    JobResumesComponent,
    JobSettingsComponent,
    RegisterComponent,
    ForgotPasswordComponent,
    PageNotFoundComponent,
    PageNotWorkingComponent,
    FontAwesomeComponent,
    FeatherComponent,
    LineIconComponent,
    FlagIconComponent,
    PaymentIconComponent,
    GalleryComponent,
    CardComponent,
    CardImagesComponent,
    DataComponent,
    SocialComponent,
    CalendarComponent,
    ChatComponent,
    ContactInfoComponent,
    FileManagerComponent,
    PageSearchComponent,
    AppSettingComponent,
    PageProfileComponent,
    VehiclesComponent,
    DeliveryRequestSearchComponent,
    NewDeliveryRequestComponent,
    ViewDeliveryRequestComponent,
    AdminDashboardComponent,
    DefaultDashboardComponent,
    VendorDashboardComponent,
    PartnerDashboardComponent,
    LeftSidebarComponent,
    HeaderTopComponent,
    PageTopbarComponent,
    UpdatePasswordComponent,
    NewAdminDeliveryRequestComponent,
    ViewAdminDeliveryRequestWidgetComponent,
    ViewDeliveryRequestWidgetComponent,
    WebsiteComponent,
    DriverDashboardComponent,
    ViewDriverDeliveryRequestWidgetComponent,
    DeliveryStatusUpdateWidgetComponent,
    PaymentComponent,
    PaymentConfirmationComponent,
    PaymentFailedComponent,
    HomepageComponent,
    HomeComponent,
    WebViewDeliveryRequestComponent,
    PricingComponent,
    FaqsComponent,
    TermsComponent,
    ShippingPolicyComponent,
    PartnerComponent,
    PartnerMinivanComponent,
    FcmNotificationComponent,
    DriverProfileComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    RichTextEditorAllModule,
    FullCalendarModule,
    NgApexchartsModule,
    ToastrModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDs7r-H-p91b7yOY9SLKJpRIFol7oDGi6U'
    }),
    NgMultiSelectDropDownModule.forRoot(),
    HttpClientModule,
    PaginationModule.forRoot(),
    ModalModule.forRoot(),
    GooglePlaceModule,
    NgIdleKeepaliveModule.forRoot(),
    MomentModule,
    AngularRaveModule.forRoot({
      key: 'FLWPUBK_TEST-c65ea2c0562b85f75bbf448ebebbeeb5-X',
      isTest: true,
    }),
    AutocompleteLibModule,

    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFireMessagingModule,
    AngularFireModule.initializeApp(environment.firebase)
  ],
  providers: [httpInterceptorProviders, LoggedInGuardGuard, MessagingService, AsyncPipe ],
  bootstrap: [AppComponent],
  entryComponents: [],
  exports: []
})
export class AppModule { }
