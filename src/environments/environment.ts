// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  baseUrl: 'http://192.168.0.179:8083',
  sessionTimeout: 30,
  flutterwavePubKey: "FLWPUBK_TEST-c65ea2c0562b85f75bbf448ebebbeeb5-X",
  firebase: {
    apiKey: "AIzaSyDs7r-H-p91b7yOY9SLKJpRIFol7oDGi6U",
    authDomain: "naytifbox-1571828714198.firebaseapp.com",
    databaseURL: "https://naytifbox-1571828714198.firebaseio.com",
    projectId: "naytifbox-1571828714198",
    storageBucket: "naytifbox-1571828714198.appspot.com",
    messagingSenderId: "510645093497",
    appId: "1:510645093497:web:3bb55cf37d2c80a3ea00a8",
    measurementId: "G-XTNV7W6647"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
